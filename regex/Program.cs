﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace regex
{
	class Program
	{
		const string testLink = "https://mattweb.ru/cheatsheets/item/172-regulyarnye-vyrazheniya-javascript-cheatsheet";

		static void Main(string[] args)
		{
			var uri = (args != null && args.Length != 0) ? args[0] : testLink;
			ShowLinksInHtmlByUri(uri);
		}

		static void ShowLinksInHtmlByUri(string uri)
		{
			var htmlLoader = new HtmlLoader(uri);
			var linkFinder = new LinkFinder(htmlLoader.Html);
			var linksAsText = string.Join(Environment.NewLine, linkFinder.Links);
			Console.WriteLine(linksAsText);
			Console.WriteLine();
			Console.WriteLine("Press any key to exit");
			Console.ReadKey();
		}
	}
}
