﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace regex
{
	class LinkFinder
	{
		public List<string> Links { get; set; }
		private const string htmlLinkPattern = "(?<link>https?:\\/\\/[^\"'\\s<]+\\.[^\"'\\s<]+)";
		//private const string htmlLinkPattern = "href\\s?=\\s?[\"'](?<link>https?:\\/\\/[^\"']+)";
		//private const string htmlLinkPattern2 = "href\\s?=\\s?\"(?<link>https?:\\/\\/.+?)";//Становится ленивым с ?
		public LinkFinder(string text)
		{
			if (text == null)
				throw new ArgumentNullException(nameof(text));

			Links = new List<string>();

			var matchCollection = Regex.Matches(text, htmlLinkPattern);

			var fM = matchCollection[0].Groups["link"].Value;


			foreach (Match match in matchCollection)
			{
				var link = match.Groups["link"].Value;
				Links.Add(link);
			}
		}
	}
}
