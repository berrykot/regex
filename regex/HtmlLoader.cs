﻿using System;
using System.Net;
using System.Linq;
using System.Text;

namespace regex
{
	class HtmlLoader
	{
		public string Html { get; private set; }
		public HtmlLoader(string uri)
		{
			using (WebClient client = new WebClient())
			{
				client.Encoding = Encoding.UTF8;
				Html = client.DownloadString(uri);
				Html = WebUtility.HtmlDecode(Html);
			}
		}
	}
}
